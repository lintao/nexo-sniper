#ifndef SNIPER_INCIDENT_MGR_H
#define SNIPER_INCIDENT_MGR_H

#include <string>
#include <map>
#include <list>

class Incident;
class IIncidentHandler;

class IncidentMgr
{
    public :

        friend class Incident;
        friend class IIncidentHandler;

        static IncidentMgr* instance();
        static void release();

    private :

        bool handle(Incident& incident);
        void regist(const std::string& incident, IIncidentHandler* handler);
        void unregist(const std::string& incident, IIncidentHandler* handler);

        const std::string& objName() { return m_name; }

        IncidentMgr();
        ~IncidentMgr();

        typedef std::list<IIncidentHandler*>  HandlerList;
        typedef std::map<std::string, HandlerList>  IncidentMap;

        std::string  m_name;
        IncidentMap  m_map;

        static IncidentMgr* s_obj;
};

#endif
