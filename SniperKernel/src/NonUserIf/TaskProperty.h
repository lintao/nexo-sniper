#ifndef SNIPER_TASK_PROPERTY_H
#define SNIPER_TASK_PROPERTY_H

#include "SniperKernel/Property.h"
#include <iostream>

class TaskProperty : public SniperProperty<std::vector<std::string> >
{
    public :

        typedef SniperProperty<std::vector<std::string> > BaseType;

        TaskProperty(const std::string&key, Task* scope)
            : BaseType(key, m_names), m_scope(scope)
        {
            if ( key == "algs") {
                pclear  = &Task::clearAlgs;
                pcreate = &TaskProperty::createAlg;
                padd    = &TaskProperty::addAlg;
            }
            else if ( key == "svcs" ) {
                pclear  = &Task::clearSvcs;
                pcreate = &TaskProperty::createSvc;
                padd    = &TaskProperty::addSvc;
            }
            else if ( key == "tasks" ) {
                pclear  = &Task::clearTasks;
                pcreate = &TaskProperty::createTask;
                padd    = &TaskProperty::addTask;
            }
            else {
                throw SniperException("Unsupported TaskProperty key name");
            }
        }

        bool set(bp::object& var) {
            if ( ! m_names.empty() ) {
                std::cout << "WARNING :: " << m_key
                          << " is not empty, potential ERRORS"
                          << std::endl;
                m_names.clear();
                (m_scope->*pclear)();
            }
            return this->append(var);
        }

        bool append(bp::object& var) {
            bp::extract<DLElement*> _var(var);
            if ( _var.check() ) { //python alg/svc/task object
                DLElement* obj = _var();
                return (this->*padd)(obj);
            }
            unsigned int begin = m_names.size();
            if ( !BaseType::append(var) ) {
                return false;
            }
            for (unsigned int i = begin; i < m_names.size(); ++i ) {
                if ( ! (this->*pcreate)(m_names[i]) ) {
                    return false;
                }
            }
            return true;
        }

    private :

        bool createAlg(const std::string& name) {
            return m_scope->createAlg(name) != 0;
        }
        bool createSvc(const std::string& name) {
            return m_scope->createSvc(name) != 0;
        }
        bool createTask(const std::string& name) {
            return m_scope->createTask(name) != 0;
        }
        bool addAlg(DLElement* alg) {
            return m_scope->addAlg(dynamic_cast<AlgBase*>(alg)) != 0;
        }
        bool addSvc(DLElement* svc) {
            return m_scope->addSvc(dynamic_cast<SvcBase*>(svc)) != 0;
        }
        bool addTask(DLElement* task) {
            return m_scope->addTask(dynamic_cast<Task*>(task)) != 0;
        }

        void (Task::*pclear)();
        bool (TaskProperty::*pcreate)(const std::string&);
        bool (TaskProperty::*padd)(DLElement*);

        Task*  m_scope;
        std::vector<std::string>  m_names;
};

#endif
