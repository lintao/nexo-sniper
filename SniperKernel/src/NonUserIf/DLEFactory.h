#ifndef SNIPER_DLE_FACTORY_H
#define SNIPER_DLE_FACTORY_H

#include <string>
#include <map>

class DLElement;

class DLEFactory
{
    public :

        typedef DLElement* (*DLECreator)(const std::string&);

        //get the singleton instance
        static DLEFactory* instance();

        //release the singleton instance
        static void release();

        //create a DLE object with its name
        DLElement* create(const std::string& name);

        //book the creator of new DLE types
        bool book(const std::string& type, DLECreator creator);

        //name
        const std::string& objName() { return m_name; }

    private :

        typedef std::map<std::string, DLECreator> Type2CreatorMap;

        //standard constructor
        DLEFactory();
        ~DLEFactory();

        //members
        std::string      m_name;
        Type2CreatorMap  m_creators;

        static DLEFactory* s_obj;
};

#endif
