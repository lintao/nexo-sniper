#include "SniperKernel/ToolBase.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct ToolBaseWarp : ToolBase, bp::wrapper<ToolBase>
{
    ToolBaseWarp(const std::string& name)
        : ToolBase(name)
    {
    }

    bool initialize() {
        if ( bp::override f = this->get_override("initialize") ) return f();
        return ToolBase::initialize();
    }

    bool default_initialize() {
        return this->ToolBase::initialize();
    }

    bool finalize() {
        if ( bp::override f = this->get_override("finalize") ) return f();
        return ToolBase::finalize();
    }

    bool default_finalize() {
        return this->ToolBase::finalize();
    }
};

void export_Sniper_ToolBase()
{
    using namespace boost::python;

    class_<ToolBaseWarp, bases<DLElement>, boost::noncopyable>
        ("ToolBase", init<const std::string&>())
        .def("initialize", &ToolBase::initialize, &ToolBaseWarp::default_initialize)
        .def("finalize",   &ToolBase::finalize, &ToolBaseWarp::default_finalize)
        ;
}
