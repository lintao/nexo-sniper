#include "SniperKernel/Task.h"
#include "SniperKernel/Incident.h"
#include "SniperKernel/SvcBase.h"
#include "SniperKernel/AlgBase.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct TaskWrap : Task, bp::wrapper<Task>
{
    TaskWrap(const std::string& name)
        : Task(name)
    {
    }

    bool handle(Incident& i) {
        if ( bp::override f = this->get_override("handle") ) return f(i);
        return Task::handle(i);
    }

    bool default_handle(Incident& i) {
        return this->Task::handle(i);
    }

    bool run() {
        if ( bp::override f = this->get_override("run") ) return f();
        return Task::run();
    }

    bool default_run() {
        return this->Task::run();
    }

    bool initialize() {
        if ( bp::override f = this->get_override("initialize") ) return f();
        return Task::initialize();
    }

    bool default_initialize() {
        return this->Task::initialize();
    }

    bool execute() {
        if ( bp::override f = this->get_override("execute") ) return f();
        return Task::execute();
    }

    bool default_execute() {
        return this->Task::execute();
    }

    bool finalize() {
        if ( bp::override f = this->get_override("finalize") ) return f();
        return Task::finalize();
    }

    bool default_finalize() {
        return this->Task::finalize();
    }
};

void export_Sniper_Task()
{
    using namespace boost::python;

    class_<TaskWrap, bases<DLElement, IIncidentHandler>, boost::noncopyable>
        ("Task", init<const std::string&>())
        .def("createSvc",  &Task::createSvc,
                return_value_policy<reference_existing_object>())
        .def("createAlg",  &Task::createAlg,
                return_value_policy<reference_existing_object>())
        .def("createTask", &Task::createTask,
                return_value_policy<reference_existing_object>())
        .def("find",       &Task::find,
                return_value_policy<reference_existing_object>())
        .def("handle",     &Task::handle, &TaskWrap::default_handle)
        .def("run",        &Task::run, &TaskWrap::default_run)
        .def("initialize", &Task::initialize, &TaskWrap::default_initialize)
        .def("execute",    &Task::execute, &TaskWrap::default_execute)
        .def("finalize",   &Task::finalize, &TaskWrap::default_finalize)
        .def("asTop",      &Task::asTop)
        .def("isTop",      &Task::isTop)
        .def("setEvtMax",  &Task::setEvtMax)
        .def("clearTasks", &Task::clearTasks)
        .def("clearAlgs",  &Task::clearAlgs)
        .def("clearSvcs",  &Task::clearSvcs)
        .def("remove",     &Task::remove)
        ;
}
