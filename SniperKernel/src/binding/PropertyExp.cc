#include "SniperKernel/Property.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct PropertyWrap : Property, bp::wrapper<Property>
{
    bool set(bp::object& var) {
        return this->get_override("set")(var);
    }

    bool append(bp::object& var) {
        if ( bp::override f = this->get_override("append") )
            return f(var);
        return Property::append(var);
    }

    bool default_append(bp::object& var) {
        return this->Property::append(var);
    }
};

void export_Sniper_Property()
{
    using namespace boost::python;

    class_<PropertyWrap, boost::noncopyable>("Property", no_init)
        .def("set",    pure_virtual(&Property::set))
        .def("append", &Property::append, &PropertyWrap::default_append)
        .def("show",   &Property::show)
        ;
}
