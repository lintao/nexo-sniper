#include "SniperKernel/Task.h"
#include "NonUserIf/PythonCapsul.h"
#include "NonUserIf/IncidentMgr.h"
#include "NonUserIf/DLEFactory.h"
#include <iostream>

void Sniper::python_capsul()
{
    static struct PythonCapsul
    {
        PythonCapsul() {
            std::cout << "*****************************************\n"
                      << "***     Welcome to SNiPER Python      ***\n"
                      << "*****************************************"
                      << std::endl;
            IncidentMgr::instance();
            DLEFactory::instance();
        }

        ~PythonCapsul() {
            Task::destroy_top();
            DLEFactory::release();
            IncidentMgr::release();
            std::cout << "\n***  SNiPER Terminated Successfully!  ***\n"
                      << std::endl;
        }
    } aCapsul;
}
