#include "SniperKernel/IIncidentHandler.h"
#include "SniperKernel/Incident.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct IIncidentHandlerWrap : IIncidentHandler, bp::wrapper<IIncidentHandler>
{
    bool handle(Incident& incident) {
        return this->get_override("handle")(incident);
    }
};

void export_Sniper_IIncidentHandler()
{
    using namespace boost::python;

    class_<IIncidentHandlerWrap, boost::noncopyable>("IIncidentHandler",
            no_init)
        .def("handle",    pure_virtual(&IIncidentHandler::handle))
        .def("regist",    &IIncidentHandler::regist)
        .def("unregist",  &IIncidentHandler::unregist)
        .def("listening", &IIncidentHandler::listening)
        ;
}
