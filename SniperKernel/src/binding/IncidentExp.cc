#include "SniperKernel/Incident.h"
#include <boost/python.hpp>

void export_Sniper_Incident()
{
    using namespace boost::python;

    bool (Incident::*member_fire)() = &Incident::fire;

    class_<Incident>("Incident", init<const std::string&>())
        .def("fire", member_fire)
        .def("name", &Incident::name,
                return_value_policy<copy_const_reference>())
        ;
}
