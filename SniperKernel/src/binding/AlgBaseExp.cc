#include "SniperKernel/AlgBase.h"
#include "SniperKernel/ToolBase.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct AlgBaseWarp : AlgBase, bp::wrapper<AlgBase>
{
    AlgBaseWarp(const std::string& name)
        : AlgBase(name)
    {
    }

    bool initialize() {
        return this->get_override("initialize")();
    }

    bool execute() {
        return this->get_override("execute")();
    }

    bool finalize() {
        return this->get_override("finalize")();
    }
};

void export_Sniper_AlgBase()
{
    using namespace boost::python;

    class_<AlgBaseWarp, bases<DLElement>, boost::noncopyable>
        ("AlgBase", init<const std::string&>())
        .def("initialize", pure_virtual(&AlgBase::initialize))
        .def("execute",    pure_virtual(&AlgBase::execute))
        .def("finalize",   pure_virtual(&AlgBase::finalize))
        .def("createTool", &AlgBase::createTool,
                return_value_policy<reference_existing_object>())
        .def("findTool",   &AlgBase::findTool,
                return_value_policy<reference_existing_object>())
        ;
}
