#include "SniperKernel/Sniper.h"
#include "SniperKernel/Task.h"
#include <boost/python.hpp>

void export_Sniper_Sniper()
{
    using namespace boost::python;

    def("setLogLevel", &Sniper::setLogLevel);
    def("loadDll",     &Sniper::loadDll);
    def("TopTask",     &Sniper::TopTask,
            return_value_policy<reference_existing_object>());
}
