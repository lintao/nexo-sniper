#include "SniperKernel/SvcBase.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct SvcBaseWarp : SvcBase, bp::wrapper<SvcBase>
{
    SvcBaseWarp(const std::string& name)
        : SvcBase(name)
    {
    }

    bool initialize() {
        return this->get_override("initialize")();
    }

    bool finalize() {
        return this->get_override("finalize")();
    }
};

void export_Sniper_SvcBase()
{
    using namespace boost::python;

    class_<SvcBaseWarp, bases<DLElement>, boost::noncopyable>
        ("SvcBase", init<const std::string&>())
        .def("initialize", pure_virtual(&SvcBase::initialize))
        .def("finalize",   pure_virtual(&SvcBase::finalize))
        ;
}
