#include "SniperKernel/DLElement.h"
#include "SniperKernel/Task.h"
#include <boost/python.hpp>

namespace bp = boost::python;

struct DLElementWrap : DLElement, bp::wrapper<DLElement>
{
    DLElementWrap(const std::string& name)
        : DLElement(name)
    {
    }

    bool initialize() {
        return this->get_override("initialize")();
    }

    bool finalize() {
        return this->get_override("finalize")();
    }

    void setLogLevel(int level) {
        if ( bp::override f = this->get_override("setLogLevel") ) {
            f(level);
            return;
        }
        return DLElement::setLogLevel(level);
    }

    void default_setLogLevel(int level) {
        return this->DLElement::setLogLevel(level);
    }
};

void export_Sniper_DLElement()
{
    using namespace boost::python;

    class_<DLElementWrap, boost::noncopyable>("DLElement", no_init)
        .def("initialize",   pure_virtual(&DLElement::initialize))
        .def("finalize",     pure_virtual(&DLElement::finalize))
        .def("scope",        &DLElement::scope,
                return_value_policy<copy_const_reference>())
        .def("objName",      &DLElement::objName,
                return_value_policy<copy_const_reference>())
        //.def("setScope",     &DLElement::setScope)
        .def("getScope",     &DLElement::getScope,
                return_value_policy<reference_existing_object>())
        .def("setLogLevel",  &DLElement::setLogLevel,
                &DLElementWrap::default_setLogLevel)
        .def("logLevel",     &DLElement::logLevel)
        .def("property",     &DLElement::property,
                return_value_policy<reference_existing_object>())
        //.def("switchStatus", &DLElement::switchStatus)
        .def("show",         &DLElement::show)
        ;
}
