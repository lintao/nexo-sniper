#include "SniperKernel/DLElement.h"
#include "SniperKernel/Task.h"
#include "SniperKernel/SniperLog.h"

DLElement::DLElement(const std::string& name)
    : m_par(0), m_name(name)
{
    m_logLevel = SniperLog::logLevel();
    m_pmgr = new PropertyMgr(this);
    m_stat = Loaded;
}

DLElement::~DLElement()
{
    switchStatus(Terminated);
    delete m_pmgr;
    LogTest << " teminated !" << std::endl;
}

void DLElement::setScope(Task* par)
{
    m_par   = par;
    if ( ! par->isTop() ) {
        m_scope = par->scope() + par->objName() + ':';
    }
    m_logLevel = par->logLevel();
}

void DLElement::setLogLevel(int level)
{
    m_logLevel = level;
}

Property* DLElement::property(const std::string& key)
{
    return m_pmgr->property(key);
}

int DLElement::switchStatus(RunStatus stat)
{
    //-1: Cannot switch RunStatus
    // 0: RunStatus switched, it's ready to perform user process
    // 1: RunStatus switched, users has nothing todo
    int result = 0;

    int step = (m_stat<<8) + stat;

    switch(step) {
        case 0x3030 : //Runing is the most frequently used Status
        case 0x1020 :
        case 0x2030 :
        case 0x3040 :
        case 0x4050 :
        case 0x1050 :
        case 0x2040 :
        case 0x4020 :
        case 0x9030 :
        case 0x9040 :
            result = 0; //ReadyToDo;
            break;

        case 0x1030 :
            result = this->initialize() ? 0 : -1; //ReadyToDo;
            break;

        case 0x2010 :
        case 0x2050 :
        case 0x3050 :
        case 0x9010 :
        case 0x9050 :
            result = this->finalize() ? 0 : -1; //ReadyToDo;
            break;

        case 0x3010 :
            result = this->finalize() ? 1 : -1; //NothingToDo
            break;
        case 0x1010 :
        case 0x2020 :
        case 0x3020 :
        case 0x4010 :
        case 0x4040 :
        case 0x9090 :
        case 0x9020 :
            result = 1; //NothingToDo;
            break;

        //case 0x1040 :
        //case 0x4030 :
        //case 0x50*0 :
        default :
            result = -1; //CannotToDo;
    }

    if ( result != -1 ) m_stat = stat;

    return result;
}

void DLElement::show()
{
    write_info(std::cout, 0);
}

std::ostream& DLElement::write_info(std::ostream& os, int indent)
{
    std::string showScope;
    if ( indent == 0 ) showScope = m_scope;
    make_indent(os, indent);
    os << '[' << m_tag << ']' << showScope << m_name << std::endl;

    make_indent(os, indent+1);
    os << "[ATR]LogLevel   = " << m_logLevel << std::endl;

    const PropertyMgr::PropertyMap& pmap = m_pmgr->properties();
    PropertyMgr::PropertyMap::const_iterator it = pmap.begin();
    while ( it != pmap.end() ) {
        it->second->write_info(os, indent+1);
        ++it;
    }

    return os;
}

void DLElement::make_indent(std::ostream& os, int indent)
{
    if ( indent != 0 ) {
        for ( int i = 0; i < indent-1; ++i ) {
            os << "   |  ";
        }
        os << "   +--";
    }
}
