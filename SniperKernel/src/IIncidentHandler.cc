#include "SniperKernel/IIncidentHandler.h"
#include "SniperKernel/SniperLog.h"
#include "NonUserIf/IncidentMgr.h"
#include <algorithm>

IIncidentHandler::~IIncidentHandler()
{
    std::list<std::string>::iterator it = m_msg.end();
    while ( it != m_msg.begin() ) {
        this->unregist(*(--it));
    }
}

bool IIncidentHandler::regist(const std::string& incident)
{
    std::list<std::string>::iterator it =
        std::find(m_msg.begin(), m_msg.end(), incident);

    if ( it == m_msg.end() ) {
        m_msg.push_back(incident);
        IncidentMgr::instance()->regist(incident, this);
    }
    else {
        LogInfo << "ignore already registered incident: "
                << incident << std::endl;
    }

    return true;
}

void IIncidentHandler::unregist(const std::string& incident)
{
    std::list<std::string>::iterator it =
        std::find(m_msg.begin(), m_msg.end(), incident);
    if ( it != m_msg.end() ) {
        m_msg.erase(it);
        IncidentMgr::instance()->unregist(incident, this);
    }
}

void IIncidentHandler::listening()
{
    std::cout << "listening:";
    std::list<std::string>::iterator it = m_msg.begin();
    while ( it != m_msg.end() ) {
        std::cout << "  " << *it;
        ++it;
    }
    std::cout << std::endl;
}
