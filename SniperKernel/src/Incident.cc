#include "SniperKernel/Incident.h"
#include "NonUserIf/IncidentMgr.h"

bool Incident::fire(const std::string& msg)
{
    Incident incident(msg);
    return IncidentMgr::instance()->handle(incident);
}

bool Incident::fire()
{
    return IncidentMgr::instance()->handle(*this);
}
