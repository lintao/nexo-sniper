#include "SniperKernel/DataMemSvc.h"
#include "SniperKernel/IDataBlock.h"
#include "SniperKernel/SniperLog.h"
#include "SniperKernel/SvcFactory.h"

DECLARE_SERVICE(DataMemSvc);

DataMemSvc::DataMemSvc(const std::string& name)
    : SvcBase(name)
{
}

DataMemSvc::~DataMemSvc()
{
}

IDataBlock* DataMemSvc::find(const std::string& name)
{
    std::map<std::string, IDataBlock*>::iterator it = m_mems.find(name);
    if ( it != m_mems.end() ) {
        return it->second;
    }
    return (IDataBlock*)0;
}

bool DataMemSvc::regist(const std::string& name, IDataBlock* mem)
{
    if ( m_mems.find(name) == m_mems.end() ) {
        m_mems.insert(std::make_pair(name, mem));
        return true;
    }
    LogError << name << " is already registered here!" << std::endl;
    return false;
}

bool DataMemSvc::initialize()
{
    return true;
}

bool DataMemSvc::finalize()
{
    std::map<std::string, IDataBlock*>::iterator it = m_mems.begin();
    while ( it != m_mems.end() ) {
        delete it->second;
        ++it;
    }
    m_mems.clear();

    return true;
}
