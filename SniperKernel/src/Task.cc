#include "SniperKernel/Task.h"
#include "SniperKernel/AlgBase.h"
#include "SniperKernel/SvcBase.h"
#include "SniperKernel/DataMemSvc.h"
#include "SniperKernel/Incident.h"
#include "SniperKernel/DeclareDLE.h"
#include "SniperKernel/SniperException.h"
#include "SniperKernel/SniperLog.h"
#include "NonUserIf/TaskProperty.h"
#include "NonUserIf/DLEFactory.h"
#include <algorithm>

SNIPER_DECLARE_DLE(Task);

Task::Task(const std::string& name)
    : DLElement(name),
      m_isTop(false),
      m_evtMax(-1),
      m_loop(0)
{
    if ( m_tag.empty() ) m_tag = "Task";
    m_pmgr->addProperty(new TaskProperty("algs", this));
    m_pmgr->addProperty(new TaskProperty("svcs", this));
    m_pmgr->addProperty(new TaskProperty("tasks", this));

    this->createSvc("DataMemSvc");

    regist(name);
}

Task::~Task()
{
    switchStatus(Terminated);

    //release sub-tasks, algs and svcs
    clearTasks();
    clearAlgs();
    clearSvcs();

    //clear the pack map
    m_pack.clear();

    //if this is the TopTask, reset its static reference
    if ( m_isTop ) s_top = 0;
}

DLElement* Task::find(const std::string& name)
{
    std::string path = name;
    std::string::size_type pseg = path.find(':');
    if ( pseg != std::string::npos ) {
        std::string scope = path.substr(0, pseg);
        Task* subTask = dynamic_cast<Task*>(find(scope));
        if ( subTask != 0 ) {
            path = path.substr(pseg+1, std::string::npos);
            return subTask->find(path);
        }
    }
    else {
        DLEPackMap::iterator it = m_pack.find(name);
        if ( it != m_pack.end() ) {
            return it->second.first;
        }
    }
    LogDebug << "Cann't find Object " << name << std::endl;
    return (DLElement*)0;
}

void Task::setLogLevel(int level)
{
    DLElement::setLogLevel(level);
    DLEPackMap::iterator it = m_pack.begin();
    while ( it != m_pack.end() ) {
        it->second.first->setLogLevel(level);
        ++it;
    }
}

bool Task::run()
{
    int run = switchStatus(Running);
    if ( run != 0 ) return (run > 0);

    bool stat = m_evtMax < 0;
    while ( stat || m_loop < m_evtMax ) {
        if ( m_stat == Stopped ) break;
        if ( ! execute() ) {
            LogError << "Failed to execute algorithms" << std::endl;
            m_stat = IsError;
            return false;
        }
    }
    return true;
}

struct DLEmfFunctor {
    DLEmfFunctor(bool (DLElement::*pp)()) : stat(true), pfun(pp) {}
    void operator()(DLElement* obj) { stat = stat && (obj->*pfun)(); }
    bool stat;
    bool (DLElement::*pfun)();
};

bool Task::initialize()
{
    int run = switchStatus(Initialized);
    if ( run != 0 ) return (run > 0);

    DLEmfFunctor ff(&DLElement::initialize);
    bool result =
    std::for_each(m_svcs.begin(),  m_svcs.end(),  ff).stat &&
    std::for_each(m_algs.begin(),  m_algs.end(),  ff).stat &&
    std::for_each(m_tasks.begin(), m_tasks.end(), ff).stat;

    if ( result ) {
        LogInfo << "initialized" << std::endl;
    }
    else {
        LogError << "failed to initialize!" << std::endl;
        m_stat = IsError;
    }

    return result;
}

bool Task::execute()
{
    int run = switchStatus(Running);
    if ( run != 0 ) return (run > 0);

    ++m_loop;
    LogTest << "executing " << m_loop << std::endl;

    try {
        //trigger the BeginEvent incident
        local_fire("BeginEvent");

        //executing algorithms
        for ( std::list<AlgBase*>::iterator iAlg = m_algs.begin();
                iAlg != m_algs.end();
                ++iAlg ) {
            if ( ! (*iAlg)->execute() ) {
                LogError << "execute failed!" << std::endl;
                //FIXME: this is not an elegant way...
                Incident::fire("StopRun");
            }
        }

        //trigger the EndEvent incident
        local_fire("EndEvent");
    }
    catch (SniperStopped& e) {
        if ( ! m_isTop ) throw e;
        LogInfo << "Stopping execution..." << std::endl;
    }

    return true;
}

bool Task::finalize()
{
    int run = switchStatus(Finalized);
    if ( run != 0 ) return (run > 0);

    DLEmfFunctor ff(&DLElement::finalize);
    bool result =
    std::for_each(m_tasks.rbegin(), m_tasks.rend(), ff).stat &&
    std::for_each(m_algs.rbegin(),  m_algs.rend(),  ff).stat &&
    std::for_each(m_svcs.rbegin(),  m_svcs.rend(),  ff).stat;

    if ( result ) {
        LogInfo << "finalized" << std::endl;
    }
    else {
        LogError << "failed to finalize!" << std::endl;
        m_stat = IsError;
    }

    return result;
}

bool Task::handle(Incident& incident)
{
    if ( incident.name() == m_name ) {
        return this->execute();
    }
    else if ( incident.name() == "StopRun" ) {
        LogInfo << "StopRun signal received!" << std::endl;
        m_stat = Stopped;
        throw SniperStopped();
    }
    return true;
}

void Task::asTop()
{
    if (s_top != 0) {
        LogWarn << s_top->objName()
                << " is already TopTask, will terminate it!"
                << std::endl;
        delete s_top;
    }
    s_top   = this;
    m_isTop = true;
    regist("StopRun");
}

SvcBase* Task::createSvc(const std::string& svcName)
{
    DLElement* obj = DLEFactory::instance()->create(svcName);
    if ( obj != 0 ) {
        SvcBase* result = dynamic_cast<SvcBase*>(obj);
        if ( result != 0 ) {
            if ( this->insert(result, true) ) {
                m_svcs.push_back(result);
                return result;
            }
        }
        else {
            LogFatal << obj->objName() << " cannot cast to SvcBase."
                     << std::endl;
        }
        delete obj;
    }
    return (SvcBase*)0;
}

AlgBase* Task::createAlg(const std::string& algName)
{
    DLElement* obj = DLEFactory::instance()->create(algName);
    if ( obj != 0 ) {
        AlgBase* result = dynamic_cast<AlgBase*>(obj);
        if ( result != 0 ) {
            if ( this->insert(result, true) ) {
                m_algs.push_back(result);
                return result;
            }
        }
        else {
            LogFatal << obj->objName() << " cannot cast to AlgBase."
                     << std::endl;
        }
        delete obj;
    }
    return (AlgBase*)0;
}

Task* Task::createTask(const std::string& taskName)
{
    DLElement* obj = DLEFactory::instance()->create(taskName);
    if ( obj != 0 ) {
        Task* result = dynamic_cast<Task*>(obj);
        if ( result != 0 ) {
            if ( this->insert(result, true) ) {
                m_tasks.push_back(result);
                return result;
            }
        }
        else {
            LogFatal << obj->objName() << " cannot cast to Task."
                     << std::endl;
        }
        delete obj;
    }
    return (Task*)0;
}

SvcBase* Task::addSvc(SvcBase* svc)
{
    if ( this->insert(svc, false) ) {
        m_svcs.push_back(svc);
        return svc;
    }
    return (SvcBase*)0;
}

AlgBase* Task::addAlg(AlgBase* alg)
{
    if ( this->insert(alg, false) ) {
        m_algs.push_back(alg);
        return alg;
    }
    return (AlgBase*)0;
}

Task* Task::addTask(Task* task)
{
    if ( this->insert(task, false) ) {
        m_tasks.push_back(task);
        return task;
    }
    return (Task*)0;
}

void Task::clearTasks()
{
    //release sub-tasks
    std::list<Task*>::iterator iTask = m_tasks.end();
    while ( iTask != m_tasks.begin() ) {
        --iTask;
        DLEPackMap::iterator ii = m_pack.find((*iTask)->objName());
        if ( (*ii).second.second ) delete *iTask;
        m_pack.erase(ii);
    }
    m_tasks.clear();
}

void Task::clearAlgs()
{
    //release algs
    std::list<AlgBase*>::iterator iAlg = m_algs.end();
    while ( iAlg != m_algs.begin() ) {
        --iAlg;
        DLEPackMap::iterator ii = m_pack.find((*iAlg)->objName());
        if ( (*ii).second.second ) delete *iAlg;
        m_pack.erase(ii);
    }
    m_algs.clear();
}

void Task::clearSvcs()
{
    //release svcs
    std::list<SvcBase*>::iterator iSvc = m_svcs.end();
    while ( iSvc != m_svcs.begin() ) {
        --iSvc;
        DLEPackMap::iterator ii = m_pack.find((*iSvc)->objName());
        if ( (*ii).second.second ) delete *iSvc;
        m_pack.erase(ii);
    }
    m_svcs.clear();
}

struct DLEriFunctor {
    DLEriFunctor(const std::string& name) : stat(false), _name(name) {}
    bool operator()(DLElement* obj) {
        if ( ! stat && obj->objName() == _name) {
            stat = true;
            return true;
        }
        return false;
    }
    bool  stat;
    const std::string& _name;
};

void Task::remove(const std::string& name)
{
    DLElement* obj = this->find(name);
    Task* par = 0;

    if ( obj != 0 && (par = obj->getScope()) != 0) {
        DLEriFunctor  ff(obj->objName());
        par->m_tasks.remove_if(ff);
        if ( ! ff.stat ) par->m_algs.remove_if(ff);
        if ( ! ff.stat ) par->m_svcs.remove_if(ff);
        DLEPackMap::iterator ii = par->m_pack.find(obj->objName());
        if ( (*ii).second.second ) delete obj;
        par->m_pack.erase(ii);
        return;
    }
    LogError << "Cannot remove, no such element" << std::endl;
}

struct DLEwiFunctor {
    DLEwiFunctor(std::ostream& os, int indent) : _os(os), _indent(indent) {}
    void operator()(DLElement* obj) { obj->write_info(_os, _indent); }
    std::ostream& _os;
    int           _indent;
};

std::ostream& Task::write_info(std::ostream& os, int indent)
{
    make_indent(os, indent);
    os << '[' << m_tag << ']' << m_scope << m_name << std::endl;

    make_indent(os, indent+1);
    os << "[ATR]LogLevel   = " << m_logLevel << std::endl;
    make_indent(os, indent+1);
    os << "[ATR]IsTop      = " << m_isTop << std::endl;
    make_indent(os, indent+1);
    os << "[ATR]EvtMax     = " << m_evtMax << std::endl;

    DLEwiFunctor  ff(os, indent+1);
    for_each(m_svcs.begin(), m_svcs.end(), ff);
    for_each(m_algs.begin(), m_algs.end(), ff);
    for_each(m_tasks.begin(), m_tasks.end(), ff);

    return os;
}

bool Task::insert(DLElement* obj, bool owned)
{
    if ( m_pack.find(obj->objName()) == m_pack.end() ) {
        obj->setScope(this);
        m_pack.insert(std::make_pair(
                          obj->objName(),
                          std::make_pair(obj, owned)
                          ));
        return true;
    }
    LogFatal << "Duplicated object name " << obj->objName() << std::endl;
    return false;
}

bool Task::local_fire(const std::string& msg)
{
    if ( m_isTop ) {
        return Incident::fire(msg);
    }
    else {
        return Incident::fire(m_scope+m_name+':'+msg);
    }
}

Task* Task::s_top = 0;

Task* Task::top()
{
    if ( s_top != 0 ) {
        return s_top;
    }
    throw SniperException("No TopTask now...\n"
            "Please set a TopTask before access it");
}

void Task::destroy_top()
{
    if ( s_top ) {
        delete s_top;
        s_top = 0;
    }
}
