#include "SniperKernel/Property.h"

const std::string Property::badType = "Cann't match type for property ";
const std::string Property::cannotAppend = "Cann't append to property ";

Property::Property(const std::string& key)
    : m_key(key)
{
}

Property::~Property()
{
}

bool Property::append(bp::object& /*var*/) {
    throw SniperException(cannotAppend+m_key);
}

void Property::make_indent(std::ostream& os, int indent)
{
    if ( indent != 0 ) {
        for ( int i = 0; i < indent-1; ++i ) {
            os << "   |  ";
        }
        os << "   +--";
    }
}
