#include "SniperKernel/DeclareDLE.h"
#include "NonUserIf/DLEFactory.h"

SniperBookDLE::SniperBookDLE(const std::string& type, DLECreator creator)
{
    DLEFactory::instance()->book(type, creator);
}
