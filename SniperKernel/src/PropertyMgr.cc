#include "SniperKernel/PropertyMgr.h"
#include "SniperKernel/Property.h"
#include "SniperKernel/SniperLog.h"

PropertyMgr::PropertyMgr(DLElement* par)
    : m_par(par)
{
}

PropertyMgr::~PropertyMgr()
{
    std::map<std::string, Property*>::iterator it = m_dict.end();
    while ( it != m_dict.begin() ) {
        delete (--it)->second;
    }
}

Property* PropertyMgr::property(const std::string& key)
{
    std::map<std::string, Property*>::iterator it = m_dict.find(key);
    if ( it != m_dict.end() ) {
        return it->second;
    }
    LogWarn << "Cann't find property " << key << std::endl;
    return (Property*)0;
}

bool PropertyMgr::addProperty(Property* property)
{
    std::string key = property->key();
    std::map<std::string, Property*>::iterator it = m_dict.find(key);
    if ( it == m_dict.end() ) {
        m_dict[key] = property;
        return true;
    }
    throw SniperException("duplicated property declaration");
}
