#include "SniperKernel/SniperLog.h"

namespace SniperLog{
    int LogLevel = 3;
}

int SniperLog::logLevel()
{
    return SniperLog::LogLevel;
}

const std::string& SniperLog::scope()
{
    static std::string NonScope = "SNiPER:";
    return NonScope;
}

const std::string& SniperLog::objName()
{
    static std::string NonName = "NonName";
    return NonName;
}
