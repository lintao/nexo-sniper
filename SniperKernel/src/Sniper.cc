#include "SniperKernel/Sniper.h"
#include "SniperKernel/Task.h"
#include "SniperKernel/SniperLog.h"
#include "SniperKernel/SniperException.h"
#include <dlfcn.h>

namespace SniperLog {
    extern int LogLevel;
}

void Sniper::setLogLevel(int level)
{
    LogTest << "Set TopTask/default LogLevel" << std::endl;
    SniperLog::LogLevel = level;
    try {
        Task::top()->setLogLevel(level);
    }
    catch (SniperException&) {
        LogInfo << "No TopTask, this action will not affect existing object!"
                << std::endl;
    }
}

void Sniper::loadDll(char* dll)
{
    void *dl_handler = dlopen(dll, RTLD_LAZY|RTLD_GLOBAL);
    if ( !dl_handler ) {
        LogFatal << dlerror() << std::endl;
        std::string msg = std::string("Can't load DLL ") + dll;
        throw SniperException(msg);
    }
}

Task* Sniper::TopTask()
{
    return Task::top();
}
