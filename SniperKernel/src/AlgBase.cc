#include "SniperKernel/AlgBase.h"
#include "SniperKernel/ToolBase.h"
#include "NonUserIf/DLEFactory.h"

AlgBase::AlgBase(const std::string& name)
    : DLElement(name)
{
}

AlgBase::~AlgBase()
{
    std::map<std::string, ToolBase*>::iterator it = m_tools.begin();
    while ( it != m_tools.end() ) {
        delete (*it).second;
        ++it;
    }
}

ToolBase* AlgBase::createTool(const std::string& toolName)
{
    DLElement* obj = DLEFactory::instance()->create(toolName);
    if ( obj != 0 ) {
        ToolBase* result = dynamic_cast<ToolBase*>(obj);
        if ( result != 0 ) {
            if ( 0 == this->findTool( result->objName() ) ) {
                result->setScope(this->getScope());
                m_tools.insert(std::make_pair(result->objName(), result));
                return result;
            }
            else {
                LogError << "already exist tool: " << result->objName()
                         << std::endl;
            }
        }
        else {
            LogFatal << obj->objName() << " cannot cast to ToolBase."
                     << std::endl;
        }
        delete obj;
    }
    return (ToolBase*)0;
}

ToolBase* AlgBase::findTool(const std::string& toolName)
{
    std::map<std::string, ToolBase*>::iterator it = m_tools.find(toolName);
    if ( it != m_tools.end() ) {
        return (*it).second;
    }
    //LogInfo << "cannot find tool: " << toolName << std::endl;
    return (ToolBase*)0;
}

std::ostream& AlgBase::write_info(std::ostream& os, int indent)
{
    DLElement::write_info(os, indent);
    std::map<std::string, ToolBase*>::iterator it = m_tools.begin();
    while ( it != m_tools.end() ) {
        (*it).second->write_info(os, indent+1);
        ++it;
    }
    return os;
}
