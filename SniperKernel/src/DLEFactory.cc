#include "NonUserIf/DLEFactory.h"
#include "SniperKernel/DLElement.h"
#include "SniperKernel/SniperException.h"
#include "SniperKernel/SniperLog.h"

DLEFactory* DLEFactory::s_obj = 0;

DLEFactory* DLEFactory::instance()
{
    return (s_obj != 0) ? s_obj : (s_obj = new DLEFactory);
}

void DLEFactory::release()
{
    if ( s_obj ) {
        delete s_obj;
        s_obj = 0;
    }
}

DLElement* DLEFactory::create(const std::string& name)
{
    std::string type = name;
    std::string nobj = name;
    std::string::size_type pseg = name.find('/');
    if ( pseg != std::string::npos ) {
        type = type.substr(0, pseg);
        nobj = nobj.substr(pseg+1, std::string::npos);
    }

    Type2CreatorMap::iterator it = m_creators.find(type);
    if ( it != m_creators.end() ) {
        LogDebug << "Creating object " << type << " named "
            << nobj << std::endl;
        DLElement* result = (it->second)(nobj);
        result->setTag(type);
        return result;
    }

    LogError << "Unknown DLE type " << type << std::endl;
    throw SniperException("Make sure you have declared the DLE type");
}

bool DLEFactory::book(const std::string& type, DLECreator creator)
{
    Type2CreatorMap::iterator it = m_creators.find(type);
    if ( it == m_creators.end() ) {
        m_creators.insert(std::make_pair(type, creator));
        LogTest << "Book DLE type: " << type << std::endl;
        return true;
    }
    LogFatal << "Duplicated type: " << type << std::endl;
    return false;
}

DLEFactory::DLEFactory()
    : m_name("DLEFactory")
{
    LogTest << "Make DLEFactory" << std::endl;
}

DLEFactory::~DLEFactory()
{
}
