#include "NonUserIf/IncidentMgr.h"
#include "SniperKernel/Incident.h"
#include "SniperKernel/IIncidentHandler.h"
#include "SniperKernel/SniperLog.h"
#include <algorithm>

IncidentMgr* IncidentMgr::s_obj = 0;

IncidentMgr* IncidentMgr::instance()
{
    return (s_obj != 0) ? s_obj : (s_obj = new IncidentMgr);
}

void IncidentMgr::release()
{
    if ( s_obj ) {
        delete s_obj;
        s_obj = 0;
    }
}

bool IncidentMgr::handle(Incident& incident)
{
    LogDebug << "Processing incident " << incident.name() << std::endl;
    IncidentMap::iterator ii = m_map.find(incident.name());
    if ( ii != m_map.end() ) {
        HandlerList& handlers = ii->second;
        for ( HandlerList::iterator ih = handlers.begin();
                ih != handlers.end();
                ++ih ) {
            bool stat = (*ih)->handle(incident);
            if ( ! stat ) {
                return false;
            }
        }
    }
    return true;
}

void IncidentMgr::regist(const std::string& incident, IIncidentHandler* handler)
{
    m_map[incident].push_back(handler);
}

void IncidentMgr::unregist(const std::string& incident, IIncidentHandler* handler)
{
    IncidentMap::iterator ii = m_map.find(incident);
    if ( ii != m_map.end() ) {
        HandlerList& handlers = ii->second;
        HandlerList::iterator ih = std::find(handlers.begin(), handlers.end(), handler);
        if ( ih != handlers.end() ) {
            handlers.erase(ih);
        }
        if ( handlers.empty() ) {
            m_map.erase(ii);
        }
    }
}

IncidentMgr::IncidentMgr()
    : m_name("IncidentMgr")
{
    LogTest << "Make IncidentMgr" << std::endl;
}

IncidentMgr::~IncidentMgr()
{
}
