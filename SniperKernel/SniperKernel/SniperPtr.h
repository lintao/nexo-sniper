#ifndef SNIPER_SNIPER_PTR_H
#define SNIPER_SNIPER_PTR_H

#include "SniperKernel/RefBase.h"
#include "SniperKernel/Task.h"
#include "SniperKernel/SniperException.h"
#include "SniperKernel/SniperLog.h"
#include <string>

//Get the DLE(Dynamically Loadable Element) reference with its path
template<typename DLE>
class SniperPtr : public RefBase<DLE>
{
    public :

        //Constructor with the DLE's path
        SniperPtr(const std::string& path);
        SniperPtr(Task* par, const std::string& path);

        //virtual ~SniperPtr();

    private :

        void init(Task* par, const std::string& path);

        //following methods are not supported
        SniperPtr();
        SniperPtr(const SniperPtr&);
        SniperPtr& operator=(const SniperPtr&);
};

template<typename DLE>
SniperPtr<DLE>::SniperPtr(const std::string& path)
{
    Task* par;
    try {
        par = Task::top();
    }
    catch(SniperException& e) {
        LogError << "TopTask=0: Please set TopTask or assign a TaskScope"
                 << std::endl;
        throw e;
    }

    init(par, path);
}

template<typename DLE>
SniperPtr<DLE>::SniperPtr(Task* par, const std::string& path)
{
    init(par, path);
}

template<typename DLE>
void SniperPtr<DLE>::init(Task* par, const std::string& path)
{
    this->m_obj = dynamic_cast<DLE*>(par->find(path));
}

#endif
