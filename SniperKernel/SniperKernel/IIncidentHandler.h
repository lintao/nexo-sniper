#ifndef SNIPER_I_INCIDENT_HANDLER_H
#define SNIPER_I_INCIDENT_HANDLER_H

#include <string>
#include <list>

class Incident;

class IIncidentHandler
{
    public :

        virtual ~IIncidentHandler();

        virtual bool handle(Incident& incident) = 0;

        bool regist(const std::string& incident);

        void unregist(const std::string& incident);

        void listening();


    private :

        std::list<std::string>  m_msg;
};

#endif
