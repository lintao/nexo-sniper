#ifndef SNIPER_DL_ELEMENT_H
#define SNIPER_DL_ELEMENT_H

#include "SniperKernel/PropertyMgr.h"
#include <string>

class Task;

// The base class of Dynamically Loadable Element
class DLElement
{
    public :

        enum RunStatus {
            Loaded       = 0x10,
            Initialized  = 0x20,
            Running      = 0x30,
            Finalized    = 0x40,
            Terminated   = 0x50,
            IsError      = 0x80,
            Stopped      = 0x90
        };

        DLElement(const std::string& name);

        virtual ~DLElement();

        virtual bool initialize() = 0;
        virtual bool finalize() = 0;

        // scope...
        const std::string& scope() { return m_scope; }

        // return the name of dynamically loadable object
        const std::string& objName() { return m_name; }

        // set the scope(Task) pointer
        void setScope(Task* scope);

        // get the task pointer
        Task* getScope() { return m_par; }

        // set log level
        virtual void setLogLevel(int level);

        // get log level
        int  logLevel() { return m_logLevel; }

        // get a property via its key-name
        Property* property(const std::string& key);

        // Switch the RunStatus, return value:
        //  -1) Cannot switch RunStatus
        //   0) RunStatus switched, it's ready to perform user process
        //   1) RunStatus switched, users has nothing todo
        int switchStatus(RunStatus stat);

        // show its information
        void show();

        // write its information to an ostream
        virtual std::ostream& write_info(std::ostream& os, int indent);

        // get/set the tag flag
        const std::string& tag() { return m_tag; }
        void setTag(const std::string& tag) { m_tag = tag; }

    protected :

        template<typename Type>
        bool declProp(const std::string& key, Type& var);

        // offer a better look for write_info
        void make_indent(std::ostream& os, int indent);

        //data members
        RunStatus     m_stat;
        int           m_logLevel;
        Task*         m_par;  //parent
        PropertyMgr*  m_pmgr;

        std::string   m_scope;
        std::string   m_name;
        std::string   m_tag; //[Task], [Alg], [Svc]

    private :
        // following interfaces are not supported
        DLElement();
        DLElement(const DLElement&);
        DLElement& operator=(const DLElement&);
};

template<typename Type>
bool DLElement::declProp(const std::string& key, Type& var)
{
    return m_pmgr->addProperty(key, var);
}

#endif
