#ifndef SNIPER_SVC_BASE_H
#define SNIPER_SVC_BASE_H

#include "SniperKernel/DLElement.h"
#include "SniperKernel/SniperLog.h"

class SvcBase : public DLElement
{
    public :

        SvcBase(const std::string& name);

        virtual ~SvcBase() {}

        //Declared in base class DLElement
        //virtual bool initialize() = 0;
        //virtual bool finalize() = 0;
};

#endif
