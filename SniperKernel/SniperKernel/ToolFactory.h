#ifndef SNIPER_TOOL_FACTORY_H
#define SNIPER_TOOL_FACTORY_H

#include "SniperKernel/DeclareDLE.h"
#define DECLARE_TOOL(ToolClass) SNIPER_DECLARE_DLE(ToolClass)

#endif //TOOL_FACTORY_H
