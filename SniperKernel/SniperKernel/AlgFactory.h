#ifndef SNIPER_ALG_FACTORY_H
#define SNIPER_ALG_FACTORY_H

#include "SniperKernel/DeclareDLE.h"
#define DECLARE_ALGORITHM(AlgClass) SNIPER_DECLARE_DLE(AlgClass)

#endif //ALG_FACTORY_H
