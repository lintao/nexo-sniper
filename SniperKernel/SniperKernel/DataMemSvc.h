#ifndef SNIPER_DATA_MEM_SVC_H
#define SNIPER_DATA_MEM_SVC_H

#include "SniperKernel/SvcBase.h"

class IDataBlock;

class DataMemSvc : public SvcBase
{
    public :

        DataMemSvc(const std::string& name);
        virtual ~DataMemSvc();

        //find a DataBlock with its name
        IDataBlock*  find(const std::string& name);

        //regist a DataBlock to this service
        bool regist(const std::string& name, IDataBlock* mem);

        //initialize and finalize
        bool initialize();
        bool finalize();


    private :

        std::map<std::string, IDataBlock*> m_mems;
};

#endif
