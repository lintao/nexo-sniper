#ifndef SNIPER_REF_BASE_H
#define SNIPER_REF_BASE_H

template<typename Type>
class RefBase
{
    public :

        //Standard constructor
        RefBase()  : m_obj(0) {}

        //Constructor with initialization
        RefBase(Type* pObj)  : m_obj(pObj) {}

        //Constructor with initialization from const object
        RefBase(const Type* pObj)  : m_obj(const_cast<Type*>(pObj)) {}

        //Copy constructor
        RefBase(const RefBase& copy)  : m_obj(copy.m_obj) {}

        //Assignment operator
        RefBase<Type>& operator=(const RefBase<Type>& right) {
            m_obj = right.m_obj;
            return *this;
        }

        //Setter
        void set(Type* pObj) { m_obj = pObj; }
        //Getter
        Type* data() { return m_obj; }
        //const Getter
        const Type* data() const { return m_obj; }

        //Check its validity
        bool valid() { return m_obj != 0; }
        bool invalid() { return m_obj == 0; }

        //XXX: users have to ensure m_obj's validity before dereference opration
        //Dereference operator ->
        Type* operator->() { return m_obj; }
        //const Dereference operator ->
        const Type* operator->() const { return m_obj; }
        //Dereference operator *
        Type& operator*() { return *m_obj; }
        //const Dereference operator *
        const Type& operator*() const { return *m_obj; }


    protected :

        Type* m_obj;
};

#endif
