#ifndef SNIPER_SNIPER_DATA_PTR_H
#define SNIPER_SNIPER_DATA_PTR_H

#include "SniperKernel/RefBase.h"
#include "SniperKernel/Task.h"
#include "SniperKernel/DataMemSvc.h"
#include "SniperKernel/SniperException.h"
#include "SniperKernel/SniperLog.h"
#include <string>

//Get DataBlock reference with its path
template<typename Data>
class SniperDataPtr : public RefBase<Data>
{
    public :

        SniperDataPtr(const std::string& path);
        SniperDataPtr(Task* par, const std::string& path);

        //virtual ~SniperDataPtr();

    private :

        void init(Task* par, const std::string& path);

        //following methods are not supported
        SniperDataPtr(const SniperDataPtr&);
        SniperDataPtr& operator=(const SniperDataPtr&);
};

template<typename Data>
SniperDataPtr<Data>::SniperDataPtr(const std::string& path)
{
    Task* par;
    try {
        par = Task::top();
    }
    catch(SniperException& e) {
        LogError << "TopTask=0: Please set TopTask or assign a TaskScope"
                 << std::endl;
        throw e;
    }

    init(par, path);
}

template<typename Data>
SniperDataPtr<Data>::SniperDataPtr(Task* par, const std::string& path)
{
    init(par, path);
}

template<typename Data>
void SniperDataPtr<Data>::init(Task* par, const std::string& path)
{
    Task* _par = par;
    std::string name = path;

    std::string::size_type pseg = path.rfind(":");
    if ( pseg != std::string::npos ) {
        _par = dynamic_cast<Task*>( par->find(path.substr(0, pseg)) );
        name = path.substr(pseg+1, std::string::npos);
    }

    DataMemSvc* svc = dynamic_cast<DataMemSvc*>(_par->find("DataMemSvc"));
    this->m_obj = (svc!=0) ? dynamic_cast<Data*>(svc->find(name)) : 0;
}

#endif
