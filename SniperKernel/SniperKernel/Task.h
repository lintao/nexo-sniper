#ifndef SNIPER_TASK_H
#define SNIPER_TASK_H

#include "SniperKernel/DLElement.h"
#include "SniperKernel/IIncidentHandler.h"
#include <list>
#include <map>

//Pre-declarations
class SvcBase;
class AlgBase;

//Task definition
class Task : public DLElement, public IIncidentHandler
{
    public :

        //Constructor
        Task(const std::string& name);

        //Destructor
        virtual ~Task();

        //find element with its name
        DLElement* find(const std::string& name);

        //set the log level of this scope
        virtual void setLogLevel(int level);

        //run this task
        virtual bool run();

        //the regular interfaces
        virtual bool initialize();
        virtual bool execute();
        virtual bool finalize();

        //handle incidents
        virtual bool handle(Incident& incident);

        //set and get the max event number to be processed
        void setEvtMax(int evtMax) { m_evtMax = evtMax; }
        int  evtMax() { return m_evtMax; }

        //make this task as TopTask
        void asTop();
        //whether this task is TopTask
        bool isTop() { return m_isTop; }

        //create concrete Svc/Alg/Task with name
        SvcBase* createSvc(const std::string& svcName);
        AlgBase* createAlg(const std::string& algName);
        Task*    createTask(const std::string& taskName);

        //add concrete Svc/Alg/Task to Task
        SvcBase* addSvc(SvcBase* svc);
        AlgBase* addAlg(AlgBase* alg);
        Task*    addTask(Task* task);

        //clear svcs/algs/tasks individually
        void clearTasks();
        void clearAlgs();
        void clearSvcs();

        //remove an element with its name
        void remove(const std::string& name);

        //override write_info from base class DLElement
        virtual std::ostream& write_info(std::ostream& os, int indent);

    protected :

        bool insert(DLElement* obj, bool owned);
        bool local_fire(const std::string& msg);

        //member data
        bool                 m_isTop; //TopTask flag
        int                  m_evtMax;
        int                  m_loop;
        std::list<SvcBase*>  m_svcs;
        std::list<AlgBase*>  m_algs;
        std::list<Task*>     m_tasks;

    private :

        typedef std::map<std::string,
                    std::pair<DLElement*, bool> >  DLEPackMap;
        DLEPackMap  m_pack;

        static Task* s_top;

    public :

        static Task* top();
        static void  destroy_top();
};

#endif
