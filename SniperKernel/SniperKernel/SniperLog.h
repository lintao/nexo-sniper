#ifndef SNIPER_SNIPER_LOG_H
#define SNIPER_SNIPER_LOG_H

#include <string>
#include <iostream>
#include <iomanip>

namespace SniperLog
{
    //2:debug, 3:info, 4:warn, 5:error, 6:fatal
    int logLevel();

    //return the global scope and objName string
    const std::string& scope();
    const std::string& objName();
}

using SniperLog::logLevel;
using SniperLog::scope;
using SniperLog::objName;

/* FIXME :
 * SniperLog is MACRO defines of if(...), so situations similar to
 *
 *        if (...)
 *            LogDebug << message1;
 *        else 
 *            LogInfo  << message2;
 *
 * will NOT WORK AS OUR WISH !!!
 *
 * Please don't forget { } in this case:
 *
 *        if (...) {
 *            LogDebug << message1;
 *        }
 *        else {
 *            LogInfo  << message2;
 *        }
 */

#define SNIPERLOG(LogLevel, LogFlag)  if ( logLevel() <= LogLevel ) \
    std::cout << std::setiosflags(std::ios::left) << std::setw(28) \
              << (scope()+objName()+'.'+__func__) << "  "#LogFlag": "

#define LogTest   SNIPERLOG(0, _TEST)
#define LogDebug  SNIPERLOG(2, DEBUG)
#define LogInfo   SNIPERLOG(3, _INFO)
#define LogWarn   SNIPERLOG(4, _WARN) 
#define LogError  SNIPERLOG(5, ERROR) 
#define LogFatal  SNIPERLOG(6, FATAL) 

#endif
