#ifndef SNIPER_INCIDENT_H
#define SNIPER_INCIDENT_H

#include <string>

class Incident
{
    public :

        static bool fire(const std::string& msg);

        Incident(const std::string& msg)
            : m_msg(msg) {}

        virtual ~Incident() {}

        virtual bool fire();

        const std::string& name() { return m_msg; }


    protected :

        std::string  m_msg;

        //not provided
        Incident();
};

#endif
