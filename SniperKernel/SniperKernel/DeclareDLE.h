#ifndef SNIPER_DECLARE_DLE_H
#define SNIPER_DECLARE_DLE_H

#include <string>

class DLElement;

struct SniperBookDLE
{
    //XXX: might functor is better ?
    typedef DLElement* (*DLECreator)(const std::string&);
    SniperBookDLE(const std::string& type, DLECreator creator);
};

#define SNIPER_DECLARE_DLE(DLEClass) \
DLElement* sniper_DLE_##DLEClass##_creator_(const std::string& name) { \
   return new DLEClass(name); \
} \
SniperBookDLE sniper_book_DLE_##DLEClass##_(#DLEClass, &sniper_DLE_##DLEClass##_creator_)

#endif
