#ifndef SNIPER_TOOL_BASE_H
#define SNIPER_TOOL_BASE_H

#include "SniperKernel/DLElement.h"
#include "SniperKernel/SniperLog.h"

class ToolBase : public DLElement
{
    public :

        ToolBase(const std::string& name);

        virtual ~ToolBase() {}

        virtual bool initialize();
        virtual bool finalize();
};

#endif
