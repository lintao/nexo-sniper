#ifndef SNIPER_SNIPER_H
#define SNIPER_SNIPER_H

class Task;

namespace Sniper {

    void setLogLevel(int level);

    void loadDll(char* dll);

    Task* TopTask();
}

#endif
