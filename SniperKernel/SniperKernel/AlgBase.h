#ifndef SNIPER_ALG_BASE_H
#define SNIPER_ALG_BASE_H

#include "SniperKernel/DLElement.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperDataPtr.h"
#include "SniperKernel/SniperLog.h"

class ToolBase;

class AlgBase : public DLElement
{
    public :

        AlgBase(const std::string& name);

        virtual ~AlgBase();

        virtual bool execute() = 0;

        //for handling tools if needed
        ToolBase* createTool(const std::string& toolName);
        ToolBase* findTool(const std::string& toolName);

        //template version of retrieving a Tool instance
        template<typename Type>
        Type* tool(const std::string& toolName);

        //Declared in base class DLElement
        //virtual bool initialize() = 0;
        //virtual bool finalize() = 0;

        virtual std::ostream& write_info(std::ostream& os, int indent);

    protected :

        std::map<std::string, ToolBase*>  m_tools;
};

template<typename Type>
Type* AlgBase::tool(const std::string& toolName)
{
    return dynamic_cast<Type*>(this->findTool(toolName));
}

#endif
