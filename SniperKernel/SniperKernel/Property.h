#ifndef SNIPER_PROPERTY_H
#define SNIPER_PROPERTY_H

#include "SniperKernel/SniperException.h"
#include <boost/python.hpp>
#include <boost/python/stl_iterator.hpp>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>

namespace bp = boost::python;

class Property
{
    public :

        Property(const std::string& key);

        virtual ~Property();

        //return the property key name
        const std::string& key() {return m_key; }

        //set property value as var
        virtual bool set(bp::object& var) = 0;

        //append var to vector/map property
        virtual bool append(bp::object& var);

        //show the value of the property
        void show() { write_info(std::cout, 0); }


        //write the value to an ostream
        virtual std::ostream& write_info(std::ostream& os, int indent) = 0;

    protected :

        // offer a better look for write_info
        void make_indent(std::ostream& os, int indent);

        const std::string  m_key;

        static const std::string badType;
        static const std::string cannotAppend;
};

template<typename T>
class SniperProperty : public Property
{
    public :

        SniperProperty(const std::string& key, T& var)
            : Property(key), m_var(var) {}

        bool set(bp::object& var)
        {
            bp::extract<T> _var(var);
            if ( _var.check() ) {
                m_var = _var();
                return true;
            }
            throw SniperException(badType+m_key);
        }


        std::ostream& write_info(std::ostream& os, int indent) {
            make_indent(os, indent);
            return os  << "[Var]" << std::setiosflags(std::ios::left)
                << std::setw(10) << m_key << " = " << m_var << std::endl;
        }

    private :

        T& m_var;
};

template<typename T>
class SniperProperty<std::vector<T> > : public Property
{
    public :

        SniperProperty(const std::string& key, std::vector<T>& var)
            : Property(key), m_var(var) {}

        bool set(bp::object& var) {
            m_var.clear();
            return this->append(var);
        }

        bool append(bp::object& var) {
            bp::extract<T> _var(var);
            if ( _var.check() ) {
                m_var.push_back(_var());
                return true;
            }
            bp::stl_input_iterator<T> begin(var), end;
            m_var.insert(m_var.end(), begin, end);
            return true;
        }

        std::ostream& write_info(std::ostream& os, int indent) {
            make_indent(os, indent);
            os << "[Var]" << std::setiosflags(std::ios::left)
                << std::setw(10) << m_key << " = [";
            if ( !m_var.empty() ) {
                os << m_var[0];
                for (unsigned int i = 1; i < m_var.size(); ++i ) {
                    os << ", " << m_var[i];
                }
            }
            return os << ']' << std::endl;
        }

    private :

        std::vector<T>& m_var;
};

template<typename K, typename V>
class SniperProperty<std::map<K, V> > : public Property
{
    public:

        SniperProperty(const std::string& key, std::map<K, V>& var)
            : Property(key), m_var(var) {}

        bool set(bp::object& var) {
            m_var.clear();
            return this->append(var);
        }

        bool append(bp::object& var) {
            bp::extract<bp::dict> dvar_(var);
            if ( dvar_.check() ) {
                bp::dict dvar = dvar_();
                bp::list iterkeys = (bp::list)dvar.iterkeys();
                bp::ssize_t dsize = bp::len(iterkeys);
                for(bp::ssize_t i = 0; i < dsize; ++i) {
                    bp::object kobj = iterkeys[i];
                    bp::extract<K> kvar(kobj);
                    bp::object vobj = dvar[kobj];
                    bp::extract<V> vvar(vobj);
                    //XXX... check the data type ???
                    m_var.insert(std::make_pair(kvar(), vvar()));
                }
            }
            return true;
        }

        std::ostream& write_info(std::ostream& os, int indent) {
            make_indent(os, indent);
            os << "[Var]" << std::setiosflags(std::ios::left)
                << std::setw(10) << m_key << " = {";
            if ( ! m_var.empty() ) {
                typename std::map<K, V>::iterator it = m_var.begin();
                os << it->first << ':' << it->second;
                for ( ++it; it != m_var.end(); ++it ) {
                    os << ", " << it->first << ':' << it->second;
                }
            }
            return os << '}' << std::endl;
        }

    private :

        std::map<K, V>& m_var;
};

///XXX: do we need ?
//class SniperProperty<std::map<K, std::vector<V> > >;
//class SniperProperty<std::map<K, std::map<VK, VV> > >;

#endif
