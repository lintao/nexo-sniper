#include <iostream>
#include <string>
#include "boost/noncopyable.hpp"
#include "boost/make_shared.hpp"
#include "boost/python.hpp"
namespace bp = boost::python;

#include "SniperKernel/SvcBase.h"
#include "RootWriter/RootWriter.h"

BOOST_PYTHON_MODULE(libRootWriter)
{
  bp::class_<RootWriter, boost::shared_ptr<RootWriter>, bp::bases<SvcBase>, boost::noncopyable>
        ("RootWriter", bp::init<std::string>())
        .def("attach", &RootWriter::attach_py)
    ;
}
